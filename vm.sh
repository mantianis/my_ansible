#!/bin/sh
if [ -f '/root/.ssh/id_rsa.pub' ] ; then
      rm /root/.ssh/id_rsa.pub
      rm /root/.ssh/id_rsa
      rm /root/.ssh/known_hosts
      rm /root/.ssh/known_hosts.old
      service ssh restart
      echo "Old ssh key was deleted"
fi
ssh-add
CUSER=maku3940
CPASS=ISIRASYK SAVO PASS!!!!!
CENDPOINT=https://grid5.mif.vu.lt/cloud3/RPC2
echo "Creating webserver virtual machine"
CVMREZ=$(onetemplate instantiate "ubuntu-16.04"  --user $CUSER --password $CPASS  --endpoint $CENDPOINT --raw TCP_PORT_FORWARDING=80)
CVMID1=$(echo $CVMREZ |cut -d ' ' -f 3)
echo $CVMID1
echo "Waiting for VM to RUN 30 sec."
sleep 30
$(onevm show $CVMID1 --user $CUSER --password $CPASS  --endpoint $CENDPOINT >$CVMID1.txt)
CSSH_CON=$(cat $CVMID1.txt | grep CONNECT\_INFO1| cut -d '=' -f 2 | tr -d '"'|sed 's/'$CUSER'/root/')
CSSH_PRIP=$(cat $CVMID1.txt | grep PRIVATE\_IP| cut -d '=' -f 2 | tr -d '"')
CSSH_IP=$(cat $CVMID1.txt | grep PUBLIC\_IP| cut -d '=' -f 2 | tr -d '"')
echo "Connection string: $CSSH_CON"
echo "Local IP: $CSSH_PRIP"
echo "[webservers]" > hosts
echo "ubuntu-16.04-$CVMID1" >> hosts
echo "$CSSH_PRIP" > ~/my_ansible/web-ip.txt
echo "$CSSH_IP" > ~/my_ansible/pubip.txt

echo "Creating database virtual machine"
CVMREZ=$(onetemplate instantiate "ubuntu-16.04"  --user $CUSER --password $CPASS  --endpoint $CENDPOINT)
CVMID2=$(echo $CVMREZ |cut -d ' ' -f 3)
echo $CVMID2
echo "Waiting for VM to RUN 30 sec."
sleep 30
$(onevm show $CVMID2 --user $CUSER --password $CPASS  --endpoint $CENDPOINT >$CVMID2.txt)
CSSH_CON=$(cat $CVMID2.txt | grep CONNECT\_INFO1| cut -d '=' -f 2 | tr -d '"'|sed 's/'$CUSER'/root/')
CSSH_PRIP=$(cat $CVMID2.txt | grep PRIVATE\_IP| cut -d '=' -f 2 | tr -d '"')
echo "Connection string: $CSSH_CON"
echo "Local IP: $CSSH_PRIP"
echo "[dbservers]" >> hosts
echo "ubuntu-16.04-$CVMID2" >> hosts
echo "$CSSH_PRIP" > ~/my_ansible/db-ip.txt

echo "Creating client virtual machine"
CVMREZ=$(onetemplate instantiate "ubuntu-16.04-byg"  --user $CUSER --password $CPASS  --endpoint $CENDPOINT)
CVMID=$(echo $CVMREZ |cut -d ' ' -f 3)
echo $CVMID
echo "Waiting for VM to RUN 30 sec."
sleep 30
$(onevm show $CVMID --user $CUSER --password $CPASS  --endpoint $CENDPOINT >$CVMID.txt)
CSSH_CON=$(cat $CVMID.txt | grep CONNECT\_INFO1| cut -d '=' -f 2 | tr -d '"'|sed 's/'$CUSER'/root/')
CSSH_PRIP=$(cat $CVMID.txt | grep PUBLIC\_IP| cut -d '=' -f 2 | tr -d '"')
TCP_PORT=$(cat $CVMID.txt | grep TCP\_PORT\_FORWARDING | cut -d '=' -f 2 | tr -d '"')
PORT=${TCP_PORT%:*}
echo "Connection string: $CSSH_CON"
echo "PUBLIC IP: $CSSH_PRIP"
echo "[clients]" >> hosts
echo "$CSSH_PRIP:$PORT" >> hosts


echo "Create new ssh key: "
ssh-keygen
cat /root/.ssh/id_rsa.pub | ssh ubuntu-16.04-$CVMID1 'cat >> .ssh/authorized_keys'
cat /root/.ssh/id_rsa.pub | ssh ubuntu-16.04-$CVMID2 'cat >> .ssh/authorized_keys'

sleep 5
ansible -m ping all

ansible-playbook database.yml
ansible-playbook server.yml
ansible-playbook client.yml
sleep 20
echo "Shutting down clients virtual machine"
onevm poweroff $CVMID --user $CUSER --password $CPASS  --endpoint $CENDPOINT
sleep 45
echo "Starting clients virtual machine"
onevm resume $CVMID --user $CUSER --password $CPASS  --endpoint $CENDPOINT
exit 0
